db.user.drop();
db.module.drop();
db.assessment.drop();
db.studentModule.drop();

var students=[{
    "_id": "100",
    "userName": "test",
    "password": "$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri",
    "associateUserId":3,
    "dateOfBirth":"2017/06/06",
    "firstName":"A",
    "lastName":"B"
    },
    {
        "_id": "101",
        "userName": "test1",
        "password": "$2y$10$9rinxw/EyUCRo26Iv.WWteWnhl4fp4VCOloi8IM/.nCB66HKPQTJK",
        "associateUserId":4,
        "dateOfBirth":"2017/06/06",
        "firstName":"A1",
        "lastName":"B1"
    }];

db.user.insert(students);
var modules=[{
        "_id":"1",
        "moduleName":"Introduction to Programming",
        "moduleDescription":"On completion of this module, students will understand how to write a basic Java Application",
        "assessment": [],
        "tags":["java","netbeans","loops","logic","branch"]
    },
    {
        "_id":"2",
        "moduleName":"Introduction to Web Development",
        "moduleDescription":"On completion of this module, students will be able to write a simple website using HTML, JS and CSS",
        "assessment": ["assess1","assess2"],
        "tags":["JavaScript","HTML","CSS","web","Static"]
    },
    {
        "_id":"3",
        "moduleName":"Digital System",
        "moduleDescription":"On completion of this module, students have basic knowledge of digital system and computer networks",
        "assessment": ["assess3","assess4"],
        "tags":["Binary","cisco","raspberry pi"]
    },
    {
        "_id":"4",
        "moduleName":"Mathematics 1",
        "moduleDescription":"On completion of this module, how linear algebra works",
        "assessment": ["assess5","assess6"],
        "tags":["Linear","Algebra"]
    },
    {
        "_id":"5",
        "moduleName":"Spreadsheet Data Analytics",
        "moduleDescription":"On completion of this module, how to use mysql to carry out some data analysis",
        "assessment": ["assess7"],
        "tags":["Data","SQL"]
    }

];
db.module.insert(modules);

var assessments=[
    {
        "_id":"assess1",
        "assessmentName":"Class test",
        "weight":"0.5",
        "topicsCovered":["Java","arithmetic"],
        "assessmentType":"CLASSTEST"
    },
    {
        "_id":"assess2",
        "assessmentName":"Project Math Quiz App",
        "weight":"0.5",
        "topicsCovered":["Java","Array"],
        "assessmentType":"LAB"
    },
    {
        "_id":"assess3",
        "assessmentName":"Raspberry Pi",
        "weight":"0.4",
        "topicsCovered":["Pi","Written"],
        "assessmentType":"CLASSTEST"
    },
    {
        "_id":"assess4",
        "assessmentName":"Cisco Network class test",
        "weight":"0.3",
        "topicsCovered":["Cisco","Netop"],
        "assessmentType":"CLASSTEST"
    },
    {
        "_id":"assess5",
        "assessmentName":"Math Quiz",
        "weight":0.6,
        "topicsCovered":["Algebra"],
        "assessmentType":"PROJECT"
    },
    {
        "_id":"assess6",
        "assessmentName":"Project Math",
        "weight":0.4,
        "topicsCovered":["Function"],
        "assessmentType":"PROJECT"
    },
    {
        "_id":"assess7",
        "assessmentName":"Data Analytics",
        "weight":1.0,
        "topicsCovered":["SQL","Data"],
        "assessmentType":"PROJECT"
    }
];
db.assessment.insert(assessments);

var studentModules=[
    {
        "_id":"sm1",
        "studentId":"100",
        "moduleId":"2",
        "lecturerId":"2",
        "average":0,
        "assessments": [
            {"assessmentId":"assess3", "time": {"date":"2018-01-01", time:"09:00"}},
            {"assessmentId":"assess4", "time": {"date":"2018-01-01", time:"09:00"}}
        ]
    }
    ];
db.studentModule.insert(studentModules);

