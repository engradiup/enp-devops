#!/bin/bash

# Example for the Docker Hub V2 API
# Returns all imagas and tags associated with a Docker Hub user account.
# Requires 'jq': https://stedolan.github.io/jq/

# set username and password
UNAME="leminhthu96"
UPASS="huongdat"

# -------

set -e
echo

# aquire token
TOKEN=$(curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${UNAME}'", "password": "'${UPASS}'"}' https://hub.docker.com/v2/users/login/ | jq -r .token)

# get list of repositories for the user account
REPO_LIST=$(curl -s -H "Authorization: JWT ${TOKEN}" https://hub.docker.com/v2/repositories/${UNAME}/?page_size=100 | jq -r '.results|.[]|.name')

# build a list of all images & tags
for i in ${REPO_LIST}
do
  # get tags for repo
  IMAGE_TAGS=$(curl -s -H "Authorization: JWT ${TOKEN}" https://hub.docker.com/v2/repositories/${UNAME}/${i}/tags/?page_size=1 | jq -r '.results|.[]|.name')

  # build a list of images from tags
  for j in ${IMAGE_TAGS}
  do
    # add each tag to list
    FULL_IMAGE_LIST="${FULL_IMAGE_LIST} ${UNAME}/${i}:${j}"
  done
done

# output
for i in ${FULL_IMAGE_LIST}
do
  echo ${i}
  docker pull ${i}

  INPUT=${i}
  SUBSTRING=$(echo $INPUT| cut -d'/' -f 2)
  FINALSUBSTRING=$(echo $SUBSTRING| cut -d':' -f 1)
  echo $FINALSUBSTRING
  docker tag ${i} localhost:5000/$FINALSUBSTRING:latest
  docker push localhost:5000/$FINALSUBSTRING:latest

done

#credit: https://gist.github.com/kizbitz/e59f95f7557b4bbb8bf2

export COMPOSE_PATH='/home/minhthu/Engradiup/enp-devops'

docker-compose -f $COMPOSE_PATH/docker-compose.yml -f $COMPOSE_PATH/mongo.yml down
docker-compose -f $COMPOSE_PATH/docker-compose.yml -f $COMPOSE_PATH/mongo.yml build
docker-compose -f $COMPOSE_PATH/docker-compose.yml -f $COMPOSE_PATH/mongo.yml up -d
docker ps